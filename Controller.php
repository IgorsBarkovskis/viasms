<?php
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'test');
define('DB_CHAR_SET', 'utf8');
define('CHAR_SET', 'UTF-8');

class Controller
{
	private $db;
	private $bank_name;
	private $delimiter;
	private $upload_dir = LOCAL_URL.'\files\\';
	private $file_destination;

	private $line_data;
	private $payment_time;
	private $payment_amount;
	private $payment_account;
	private $payment_reason;
	private $fullname;

	public function __construct($dbHost = DB_HOST, $dbUser = DB_USER, $dbPass = DB_PASS, $dbName = DB_NAME)
	{
		$this->bank_name = isset($_POST['bank_name']) ? $_POST['bank_name'] : 'HABBA';
		$this->getDelimiter();
		$this->file_destination = $this->upload_dir.date('Y-m-d') . '_'. date('H-i-s').'-'.$this->bank_name.'.dat';

		$this->db = new mysqli($dbHost, $dbUser, $dbPass, $dbName);
		if (mysqli_connect_errno())
		{
			printf("Connection Error: %s\n", mysqli_connect_error());
			exit();
		}
		$this->db->set_charset(DB_CHAR_SET);
	}

	public function upload()
	{
		$file_content = array();

		if (!move_uploaded_file($_FILES['file']['tmp_name'], $this->file_destination))
		{
			printf("File upload Error %s\n");
			exit();
		}

		if ($this->file = file($this->file_destination, FILE_SKIP_EMPTY_LINES))
		{
			$_SESSION['current_file'] = $this->file_destination;

			$payment_id = 0;

			foreach ($this->file as $key => $line)
			{
				$exists = false;
				$this->line_data = explode($this->delimiter, mb_convert_encoding($line, CHAR_SET, 'auto'));

				if ($line_data[3] < 0)
				{
					$payment_id++;
					continue;
				}

				$this->parseLine();

				$querry = " SELECT * FROM `payments` 
							WHERE (file_date = '$this->payment_time') 
							AND (payment_amount = '$this->payment_amount') 
							AND (payment_account = '$this->payment_account') 
							AND (payment_fullname = '$this->fullname')";

				$result = $this->db->query($querry);
				$exists = ($result->fetch_assoc()) ? true : false;

				$file_content[] = array(
									'payment_id' => $payment_id++,
									'payment_time' => $this->payment_time,
									'payment_amount' => $this->payment_amount,
									'payment_account' => $this->payment_account,
									'fullname' => $this->fullname,
									'payment_reason' => $this->payment_reason,
									'exists' => $exists
								);
			}
		}
		else
		{
			printf("File reading error %s\n");
			exit();
		}

		return array(
			'response' => 'Success',
			'statement' => $file_content,
		);
	}

	public function save()
	{
		$rowId = $_POST['rowId'];

		$this->file = isset($_SESSION['current_file']) ? file($_SESSION['current_file'],FILE_SKIP_EMPTY_LINES) : null;

		if ($this->file)
		{
			$this->line_data = explode($this->delimiter, mb_convert_encoding($this->file[$rowId], 'UTF-8', 'auto'));
			$this->parseLine();

			$querry = "INSERT INTO `payments` (id, upload_time, file_date,payment_amount, payment_account,payment_reason,payment_fullname)
				VALUES(DEFAULT, DEFAULT, '$this->payment_time','$this->payment_amount','$this->payment_account','$this->payment_reason','$this->fullname')";

			if ($result = $this->db->query($querry))
			{
				return 'success';
			} 
			else
			{
				printf('Data saving error');
				exit();
			}
		}
		else
		{
			printf('Data reading error');
			exit();
		}
	}

	private function parseLine()
	{

		$payment_data = explode(';',$this->line_data[5]);
		$fullname_data = explode(' ',$payment_data[2]);
		$payment_reason_array = explode('tyt.: ', $payment_data[3]);

		$this->fullname        = mysqli_real_escape_string ($this->db, $fullname_data[2].' '.$fullname_data[3] );
		$this->payment_time    = mysqli_real_escape_string ($this->db, str_replace("/","-",$this->line_data[1]) );
		$this->payment_amount  = mysqli_real_escape_string ($this->db, str_replace(",",".",$this->line_data[3]) );
		$this->payment_reason  = mysqli_real_escape_string ($this->db, str_replace(",","",$payment_reason_array[1]) );
		$this->payment_account = mysqli_real_escape_string ($this->db, $this->line_data[2] );
	}

	private function getDelimiter()
	{
		switch ($this->bank_name) {
			case "HABBA":
				$this->delimiter = '|';
				break;
			case "RIKKO":
				$this->delimiter = '|';
				break;
			default:
				$this->delimiter = '|';
		}
	}

	public function __destruct()
	{
		mysqli_close($this->db);
	}

}