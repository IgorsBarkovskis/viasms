/*
SQLyog Professional v12.4.1 (64 bit)
MySQL - 5.7.18-0ubuntu0.16.04.1 : Database - test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`test` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `test`;

/*Table structure for table `payments` */

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `file_date` varchar(50) DEFAULT NULL,
  `payment_amount` double DEFAULT NULL,
  `payment_account` varchar(50) DEFAULT NULL,
  `payment_reason` varchar(250) DEFAULT NULL,
  `payment_fullname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `payments` */

insert  into `payments`(`id`,`upload_time`,`file_date`,`payment_amount`,`payment_account`,`payment_reason`,`payment_fullname`) values 
(1,'2017-07-11 07:20:45','02:12:2015',0.01,'51114010100000538754001001',' POTWIERDZAM UPOWA?NIENIE VIASMS PL DO ZAPYTAN W BAZACH INFORMACJI GOSP ODARCZYCH, ID 1332924 ','MARTA POTEREK'),
(2,'2017-07-11 07:21:20','02:12:2015',224,'51114010100000538754001001',' AKCEPTUJ? ZMIANY W UMOWIE ZWI?ZANE Z PLANEM SP?AT. ID KLIENTA 1188109  ','KLAUDIA JELE?'),

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
