$( document ).ready(function() {
    $('#selectAll').click(function () {
        if ($(this).is(':checked')) {
            $('tbody input').each(function () {
                $(this).prop("checked", true);
            });
        } else {
            $('tbody input').each(function () {
                $(this).prop("checked", false);
            });
        }
    });

    $('#save').click(function () {
        $('tbody input').each(function () {
            if ($(this).is(':checked')) {
                addRow($(this).data('rowid'))
            }
        });
    });

    function addRow(rowId) {
        var bank_name = $( '#bankName' ).val();

        $.ajax({
            url : 'save.php',
            type : 'POST',
            data : {
                bank_name:bank_name,
                rowId:rowId},
            success : function(data) {
                $('tr#row_'+rowId).removeClass('notexists');
                $('tr#row_'+rowId).find('input').remove();
                $('tr#row_'+rowId+' td').first().text('SAVED to DATABASE');
            }
        });
    }

    $('#uploadForm').submit(function(e) {
        e.preventDefault();

        var formData = new FormData();
        formData.append('file', $('#file')[0].files[0]);
        formData.append('bank_name', $( '#bankName' ).val());

        $.ajax({
            url : 'upload.php',
            type : 'POST',
            data : formData,
            processData: false,
            contentType: false,
            success : function(data) {
                jsonData = JSON.parse(data);
                drawUploadTable(jsonData.statement);
                if (jsonData.response === 'Invalid file extension') {
                    alert ('Invalid file extension');
                }
            }
        });
    });

    function drawUploadTable(data) {
        $.each(data, function () {
            if ($(this)[0].exists === true) {
                $('#uploaded tbody').append('<tr>'+
                    '<td>'+$(this)[0].payment_time+'</td>'+
                    '<td>'+$(this)[0].fullname+'</td>'+
                    '<td>'+$(this)[0].payment_amount+'</td>'+
                    '<td>'+$(this)[0].payment_account+'</td>'+
                    '<td></td>'+
                    '<td>'+$(this)[0].payment_time+'</td>'+
                    '<td>'+$(this)[0].fullname+'</td>'+
                    '<td>'+$(this)[0].payment_amount+'</td>'+
                    '<td colspan="2">'+$(this)[0].payment_account+'</td>'+
                '</tr>');

            }
            else {
                $('#uploaded tbody').append('<tr class="notexists" id="row_'+$(this)[0].payment_id+ '">'+
                    '<td colspan="4">NOT EXISTS</td>'+
                    '<td></td>'+
                    '<td>'+$(this)[0].payment_time+'</td>'+
                    '<td>'+$(this)[0].fullname+'</td>'+
                    '<td>'+$(this)[0].payment_amount+'</td>'+
                    '<td>'+$(this)[0].payment_account+'</td>'+
                    '<td><input type="checkbox" id="checkbox_'+$(this)[0].payment_id+'" data-rowid="'+$(this)[0].payment_id+'"></td>'+
                '</tr>');
            }

        });
    }
});