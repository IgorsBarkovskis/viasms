<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>VIA SMS task</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="/public/public.css" type="text/css" />
	<link href="http://allfont.ru/allfont.css?fonts=lucida-sans-unicode" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="/public/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="/public/main.js"></script>
</head>
<body>
	<div id="page-wrapper">
		<div id="content">
			<form method="POST" action='' name="uploadForm" id="uploadForm" enctype="multipart/form-data">
				<input type="hidden" name="MAX_FILE_SIZE" value="1024000">

					<select id="bankName" name="bankName">
						<!-- option disabled selected="selected"> Choose bank Name </option-->
						<option value="HABBA" selected="selected">Swedbank (.dat)</option>
						<option value="RIKKO">DNB Bank (.csv)</option>
					</select>

					<input type="file" id="file" name="file">

				<input type="submit" name="btnUpload" value="Upload">
			</form>
			<div id="footerTable">
				<table id="uploaded">
					<thead>
						<tr>
							<th></th>
							<th colspan="2">EXISTS</th>
							<th colspan="3"></th>
							<th colspan="2">UPLOADED</th>
							<th>select all<input type="checkbox" id="selectAll"><button id="save">Save</button></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</body>
</html>
